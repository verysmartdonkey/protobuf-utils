# from setuptools import setup
from distutils.core import setup, Extension
from nodeflux.analytics import __version__
from pip.req import parse_requirements
from pip.download import PipSession

install_reqs = parse_requirements('requirements.txt', session=PipSession())
reqs = [str(ir.req) for ir in install_reqs]

setup(
    name="nodeflux.analytics",
    version=__version__,
    packages=['nodeflux', 'nodeflux.analytics'],
    description='Nodeflux Protobuf',
    url='https://bitbucket.org/verysmartdonkey/crowd-behavior',
    author=['Rizkifika'],
    install_requires=reqs)
