# NODEFLUX PROTOBUF-UTILS
---------------------------------------------
Nodeflux Library to implement rule over MQ

### Feature
- Message queueing between process, apps, etc. 

### Installation

    ```
    $ python3 setup.py install

    ```
	
### Requirements:
+ Base docker image nf-darknet v2.2.3

### Development :
+ v1.0.0 (latest) - First creation.